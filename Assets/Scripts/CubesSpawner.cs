using UnityEngine;

public class CubesSpawner : MonoBehaviour
{
    [SerializeField] private GameObject cube;
    [SerializeField] private int cubesToSpawn;
    //spawnArea = labyrinth
    private BoxCollider spawnArea;
    private Vector3 maxSpawnPos;
    private Vector3 cubeSpawnPosition;

    void Start()
    {
        spawnArea = GetComponent<BoxCollider>();
        maxSpawnPos = new Vector3(spawnArea.bounds.size.x / 2, 0, spawnArea.bounds.size.z / 2);
        for(int i=0; i<cubesToSpawn; i++)
        {
            cubeSpawnPosition = new Vector3(Random.Range(-maxSpawnPos.x, maxSpawnPos.x), 2, Random.Range(-maxSpawnPos.z, maxSpawnPos.z));
            Instantiate(cube, cubeSpawnPosition, transform.rotation);
        }
    }
}
