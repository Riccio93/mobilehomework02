using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VJoystickHandler : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image VJoystickBackgroundImage;
    private Image VJoystickHandlerImage;
    private Vector3 inputVector;

    private void Start()
    {
        VJoystickBackgroundImage = GetComponent<Image>();
        VJoystickHandlerImage = transform.GetChild(0).GetComponent<Image>();
    }

    public void OnDrag(PointerEventData ped)
    {
        Vector2 position;
        //If I'm touching the joystick's rectangle...
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(VJoystickBackgroundImage.rectTransform, 
            ped.position, ped.pressEventCamera, out position))
        {
            position.x /= VJoystickBackgroundImage.rectTransform.sizeDelta.x;
            position.y /= VJoystickBackgroundImage.rectTransform.sizeDelta.y;
            inputVector = new Vector3(position.x * 2, 0, position.y * 2);
            //Vector normalization
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;
            //Moving the handler in the joystick's rectangle
            VJoystickHandlerImage.rectTransform.anchoredPosition = 
                new Vector3(inputVector.x * (VJoystickBackgroundImage.rectTransform.sizeDelta.x/2.5f),
                inputVector.z * (VJoystickBackgroundImage.rectTransform.sizeDelta.y/2.5f));
        }
    }

    public void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public void OnPointerUp(PointerEventData ped)
    {
        //Handler's position reset
        inputVector = Vector3.zero;
        VJoystickHandlerImage.rectTransform.anchoredPosition = Vector3.zero;
    }

    public Vector3 getInputVector()
    {
        return new Vector3(inputVector.x, 0, inputVector.z);
    }

    public Vector2 getHandlerPosition()
    {
        return VJoystickHandlerImage.rectTransform.anchoredPosition;
    }
}