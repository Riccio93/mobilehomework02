using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody mRigidbody;
    private LayerMask layerMask; //Used to consider in ray cast only the black plane
    private Rigidbody cubeRb; //Used to disable rigid body of cubes when eaten by ball
    [SerializeField] private VJoystickHandler joystick;

    private void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
        //User layer 3 = plane
        layerMask = 1 << 3;
        layerMask = ~layerMask;
    }

    private void FixedUpdate()
    {
        if(Input.touchCount > 0)
        {
            //Ball control: Virtual joystick
            if(!joystick.getHandlerPosition().Equals(Vector3.zero))
            {
                Vector3 direction = joystick.getInputVector();
                mRigidbody.AddForce(direction * speed);
            }
            //Ball control: Touch
            else
            {
                foreach (Touch touch in Input.touches)
                {
                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
                    if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
                    {
                        Vector3 hitPoint = hit.point;
                        hitPoint.Normalize();
                        mRigidbody.AddForce(hitPoint * speed);
                    }
                }
            }
        }
        //Ball control: Accelerometer
        else
        {
            Vector3 direction = new Vector3(Input.acceleration.x, 0, Input.acceleration.y);
            if (direction.sqrMagnitude > 1)
                direction.Normalize();
            mRigidbody.AddForce(direction * speed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Cube"))
        {
            cubeRb = collision.gameObject.GetComponent<Rigidbody>();
            Destroy(cubeRb);            
            collision.transform.parent = transform;
        }
    }
}